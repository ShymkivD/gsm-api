import { registerAs } from '@nestjs/config';

const { env } = process;
export const mongoConfig = registerAs('mongo', () => ({
  uri: env.MONGO_CONNECTION_URL ? env.MONGO_CONNECTION_URL : 'mongodb://localhost:27017/gsm',
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
}));
