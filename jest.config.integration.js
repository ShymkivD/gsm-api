module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: '.',
  testRegex: '.*\\.test\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: ['**/*.(t|j)s'],
  coverageDirectory: './coverage/integration',
  testEnvironment: 'node',
  roots: ['<rootDir>/src/', '<rootDir>/libs/'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputName: '/coverage/integration/junit.xml',
        addFileAttribute: 'true',
      },
    ],
  ],
  coverageReporters: ['lcov', 'text', 'cobertura'],
  moduleNameMapper: {
    '@app/domain/(.*)': '<rootDir>/libs/domain/src/$1',
    '@app/domain': '<rootDir>/libs/domain/src',
    '@app/auth/(.*)': '<rootDir>/libs/auth/src/$1',
    '@app/auth': '<rootDir>/libs/auth/src',
  },
};
