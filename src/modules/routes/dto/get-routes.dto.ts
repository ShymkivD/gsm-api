import { Transform } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class GetRoutesDto {
  @IsString()
  token: string;

  @IsString()
  @IsOptional()
  search = '';

  @IsNumber()
  @Transform(({ value }) => Number(value))
  min = 0;

  @IsNumber()
  @Transform(({ value }) => Number(value))
  max = 0;

  @IsNumber()
  @Transform(({ value }) => Number(value))
  step = 20;

  @IsNumber()
  @Transform(({ value }) => Number(value))
  shift = 0;
}
