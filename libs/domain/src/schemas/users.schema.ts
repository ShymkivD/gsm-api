import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { AuthProviders, Role } from '../enums';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  @Prop()
  name?: string;

  @Prop({ unique: true, required: true })
  uid: string;

  @Prop()
  email?: string;

  @Prop({ enum: Role, default: Role.GUEST })
  role: string;

  @Prop({ enum: AuthProviders })
  provider?: string;

  @Prop({ required: true })
  token: string;

  @Prop()
  photo?: string;

  createdAt?: Date;
  updatedAt?: Date;
}

export const UsersSchema = SchemaFactory.createForClass(User);
