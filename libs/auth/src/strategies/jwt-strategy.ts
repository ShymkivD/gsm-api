import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { PassportStrategy } from '@nestjs/passport';
import { Model } from 'mongoose';
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';

import { Role } from '@app/domain/enums';
import { User, UserDocument } from '@app/domain/schemas';

export type JwtPayload = { role: Role; uid: string; name: string; accessToken: string };

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<UserDocument>,

    configService: ConfigService,
  ) {
    super(<StrategyOptions>{
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('app').jwtSecret,
    });
  }

  async validate(payload: JwtPayload) {
    const user = await this.userModel.findOne({ uid: payload.uid });

    if (user && user.name === payload.name && user.role === payload.role && user.token === payload.accessToken) {
      return user;
    }
    throw new UnauthorizedException('Invalid credentials');
  }
}
