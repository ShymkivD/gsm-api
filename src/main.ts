import { ValidationPipe } from '@nestjs/common';
import { ConfigService, ConfigType } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { v4 as uuidv4 } from 'uuid';

import { PinoLoggerService } from '@app/domain';
import { appConfig } from '@app/domain/config';
import { GlobalExceptionFilter } from '@app/domain/filters';
import { ASYNC_STORAGE } from '@app/domain/tokens';

import { AppModule } from './app.module';

export async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const config = app.get(ConfigService);

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.MQTT,
    options: config.get('mqtt'),
  });

  app.use((req, res, next) => {
    const asyncStorage = app.get(ASYNC_STORAGE);
    const traceId = req.headers['x-request-id'] || uuidv4();
    const store = new Map().set('traceId', traceId);
    asyncStorage.run(store, () => {
      next();
    });
  });

  const pinoLogger = app.get(PinoLoggerService);
  app.useLogger(pinoLogger);
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  const { port, isProduction }: ConfigType<typeof appConfig> = config.get('app');
  if (isProduction) {
    app.useGlobalFilters(new GlobalExceptionFilter());
  }
  app.startAllMicroservices();

  await app.listen(port);

  pinoLogger.log(`Application running on ${await app.getUrl()}`);
}
bootstrap();
