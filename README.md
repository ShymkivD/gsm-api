<div>
<a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
<a href="https://github.com/mqttjs/MQTT.js/" target="blank"><img src="https://raw.githubusercontent.com/mqttjs/MQTT.js/137ee0e3940c1f01049a30248c70f24dc6e6f829/MQTT.js.png" width="320" alt="MQTT.js Logo" /></a>
</div>

---

# GSM sender over MQTT
