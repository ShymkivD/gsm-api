import { ModelDefinition } from '@nestjs/mongoose';

import { Coordinate, CoordinateDocument, CoordinatesSchema } from './coordinates.schema';
import { Route, RouteDocument, RoutesSchema } from './routes.schema';
import { User, UserDocument, UsersSchema } from './users.schema';

export const Schemas: ModelDefinition[] = [
  { name: User.name, schema: UsersSchema },
  { name: Coordinate.name, schema: CoordinatesSchema },
  { name: Route.name, schema: RoutesSchema },
];

export { Coordinate, Route, User };
export { CoordinatesSchema, RoutesSchema, UsersSchema };
export { CoordinateDocument, RouteDocument, UserDocument };
