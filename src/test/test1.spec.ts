import { appConfig } from '@app/domain/config';
import { Role } from '@app/domain/enums';
import { randomString } from '@app/domain/helpers';
import { RoutesSchema } from '@app/domain/schemas';

describe('Test of some logic', () => {
  it('test1', () => {
    appConfig();
  });

  it('test2', () => {
    expect(Role.ADMIN).toEqual('admin');
  });

  it('test6', () => {
    const length = randomString(172).length;
    expect(length).toEqual(172);
  });

  it('test6', () => {
    const length = RoutesSchema;
    // expect(length).toEqual(172);
  });
});
