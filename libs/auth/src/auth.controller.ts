/* eslint-disable @typescript-eslint/no-empty-function */
import { Controller, Get, Header, Query, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

import { User } from '@app/domain/schemas';

import { AuthService } from './auth.service';
import { CredentialsDto } from './dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get()
  checkCredentials(@Query() dto: CredentialsDto) {
    return this.authService.checkCredentials(dto);
  }

  @Get('google')
  @UseGuards(AuthGuard('google'))
  async googleAuth() {}

  @Get('github')
  @UseGuards(AuthGuard('github'))
  async githubAuth() {}

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  @Header('content-type', 'text/html')
  async googleAuthRedirect(@Req() { user }: Request) {
    return this.authService.sendCredentials(user as User);
  }

  @Get('github/callback')
  @UseGuards(AuthGuard('github'))
  @Header('content-type', 'text/html')
  async githubAuthRedirect(@Req() { user }: Request) {
    return this.authService.sendCredentials(user as User);
  }
}
