import { Controller, Logger } from '@nestjs/common';
import { Ctx, MessagePattern, MqttContext, Payload } from '@nestjs/microservices';

import { CoordinatesService } from './coordinates.service';

@Controller()
export class CoordinatesController {
  private readonly logger = new Logger(CoordinatesController.name);

  constructor(private readonly coordinatesService: CoordinatesService) {}

  @MessagePattern('location')
  saveLocation(@Payload() data: any, @Ctx() context: MqttContext) {
    if (typeof data !== 'object') {
      this.logger.error(`"${context.getTopic()}" receives invalid data => ${data}`);

      throw new Error(`Unsupported message format`);
    }

    this.logger.log(`"${context.getTopic()}" receives => ${JSON.stringify(data)}`);

    return this.coordinatesService.saveCoordinate(data);
  }
}
