import { Module } from '@nestjs/common';

import { AuthModule } from '@app/auth';
import { DomainModule } from '@app/domain';

import { ApiModules } from './modules';

@Module({
  imports: [DomainModule, AuthModule, ...ApiModules],
})
export class AppModule {}
