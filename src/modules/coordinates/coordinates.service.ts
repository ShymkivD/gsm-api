import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Coordinate, CoordinateDocument } from '@app/domain/schemas';

@Injectable()
export class CoordinatesService {
  constructor(
    @InjectModel(Coordinate.name)
    private coordinateModel: Model<CoordinateDocument>,
  ) {}

  async saveCoordinate(dto: any): Promise<Coordinate> {
    return this.coordinateModel.create(dto);
  }
}
