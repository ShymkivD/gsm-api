import { Role } from '@app/domain/enums';
import { GlobalExceptionFilter } from '@app/domain/filters';
import { randomString } from '@app/domain/helpers';

describe('Test of some logic', () => {
  it('test2', () => {
    expect(Role.ADMIN).toEqual('admin');
  });

  it('test6', () => {
    const length = randomString(172).length;
    expect(length).toEqual(172);
  });

  it('test7', () => {
    const a = new GlobalExceptionFilter();
  });
});
