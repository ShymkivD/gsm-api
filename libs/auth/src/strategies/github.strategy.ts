import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Profile, Strategy } from 'passport-github2';

import { User } from '@app/domain/schemas';

import { AuthService } from '../auth.service';

type GithubProfile = Profile & { _json: { login: string } };

@Injectable()
export class GithubStrategy extends PassportStrategy(Strategy, 'github') {
  constructor(private readonly authService: AuthService, configService: ConfigService) {
    super(configService.get('githubOAuth2'));
  }

  async validate(accessToken: string, refreshToken: string, profile: GithubProfile, done: any) {
    const { id, _json, photos, emails, provider } = profile;
    const userProfile: Partial<User> = {
      uid: `gh:${id}`,
      name: _json.login,
      photo: photos[0].value,
      email: emails[0].value,
      provider,
      token: accessToken,
    };

    const userWithToken = await this.authService.validateOAuthLogin(userProfile);
    done(null, userWithToken);
  }
}
