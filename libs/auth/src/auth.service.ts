import { Injectable } from '@nestjs/common';
import { ConfigService, ConfigType } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { appConfig } from '@app/domain/config';
import { Role } from '@app/domain/enums';
import { randomString } from '@app/domain/helpers';
import { User, UserDocument } from '@app/domain/schemas';

import { CredentialsDto } from './dto';
import { JwtPayload } from './strategies/jwt-strategy';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<UserDocument>,

    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async validateOAuthLogin(user: Partial<User>): Promise<User & { jwt: string }> {
    const userExist = await this.userModel.exists({ uid: user.uid });
    const registeredUser = userExist
      ? await this.userModel.findOneAndUpdate({ uid: user.uid }, { ...user }, { new: true })
      : await this.userModel.create({ ...user, role: Role.USER });

    const { uid, role, name, token: accessToken } = registeredUser;
    const { jwtExpireTime, jwtSecret }: ConfigType<typeof appConfig> = this.configService.get('app');
    const jwt = this.jwtService.sign({ uid, role, name, accessToken }, { secret: jwtSecret, expiresIn: jwtExpireTime });

    return { jwt, ...registeredUser.toObject({ versionKey: false }) };
  }

  async checkCredentials(dto: CredentialsDto) {
    let user: User;
    const { id: uid, token } = dto;
    const payload = this.jwtService.decode(token) as JwtPayload;
    if (payload) {
      user = await this.userModel.findOne({ uid, token: payload.accessToken });
    }

    if (!user) {
      user = await this.generateGuest();
    }

    return { user, random_url: randomString(26) };
  }

  sendCredentials(user: User & { jwt?: string }) {
    let response: { type: string; data: any } = {
      type: 'error',
      data: { statusCode: 500, message: `Authentication failed. Try again later.`, error: 'Internal Server Error' },
    };

    if (user) {
      const { uid: id, name, photo, role, jwt } = user;
      response = { type: 'oauth_login', data: { id, name, photo, role, token: jwt } };
    }

    return `
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="UTF-8">
    </head>
    <body>
      <script>
        if (window.opener) {
          window.opener.postMessage(${JSON.stringify(response)}, '*');
          window.close();
        }
      </script>
    </body>  
    </html>
    `;
  }

  async generateGuest() {
    const uid = randomString(32);
    const userExist = await this.userModel.findOne({ uid });
    if (userExist) {
      return this.generateGuest();
    }

    const token = randomString(64);
    return this.userModel.create({ uid, token, role: Role.GUEST });
  }
}
