import { Transform } from 'class-transformer';
import { IsArray, IsNumber, IsOptional, IsString } from 'class-validator';

import { Route } from '@app/domain/schemas';

export class CreateRouteDto {
  @IsString()
  title: string;

  @IsString()
  address: string;

  @IsNumber()
  @Transform(({ value }) => Number(value))
  distance: number;

  @IsArray()
  route: Route['route'];

  @IsString()
  @IsOptional()
  provider?: string;
}
