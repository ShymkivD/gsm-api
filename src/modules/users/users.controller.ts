import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { UsersService } from './users.service';

@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('google')
  @UseGuards(AuthGuard('google'))
  authWithGoogle(@Req() req) {
    return this.usersService.googleLogin(req);
  }

  @Post('auth/guest')
  @UseGuards(AuthGuard('google'))
  beGuest(@Req() req) {
    return this.usersService.googleLogin(req);
  }
}
