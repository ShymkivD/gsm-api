#!/usr/bin/env bash
 
PACKAGE_LIST="apt-utils \
        git \
        zsh \
        htop \
        curl \
        wget \
        unzip \
        tree \
        zip \
        vim \
        less \
        sudo \
        man-db"

apt-get -y install --no-install-recommends ${PACKAGE_LIST}
 