import { Prop, Schema as SchemaDecorator, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema } from 'mongoose';

import { User } from '.';

class Point {
  lng: number;
  lat: number;
}

export type RouteDocument = Route & Document;

@SchemaDecorator({ timestamps: true })
export class Route {
  @Prop()
  title: string;

  @Prop()
  distance?: number;

  @Prop()
  address: string;

  @Prop({ type: Schema.Types.ObjectId, required: true, ref: 'User' })
  user?: User;

  @Prop([Point])
  route: Point[];

  @Prop({ default: 'DEFAULT' })
  provider?: string;

  createdAt: Date;
  updatedAt: Date;
}

export const RoutesSchema = SchemaFactory.createForClass(Route);
