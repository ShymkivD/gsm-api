import { IsOptional, IsString } from 'class-validator';

export class CredentialsDto {
  @IsString()
  @IsOptional()
  id?: string;

  @IsString()
  token = '';
}
