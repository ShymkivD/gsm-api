import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';

import { DomainModule } from '@app/domain';
import { appConfig } from '@app/domain/config';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Strategies } from './strategies';

@Module({
  imports: [
    DomainModule,
    JwtModule.registerAsync({
      imports: [ConfigModule.forFeature(appConfig)],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('app').jwtSecret,
        signOptions: { expiresIn: configService.get('app').jwtExpireTime },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, ...Strategies],
})
export class AuthModule {}
