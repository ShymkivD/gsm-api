import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

import { configs, mongoConfig } from './config';
import { LoggerModule } from './modules/logger/logger.module';
import { Schemas } from './schemas';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({ load: configs }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule.forFeature(mongoConfig)],
      useFactory: async (configService: ConfigService) => configService.get('mongo'),
      inject: [ConfigService],
    }),
    LoggerModule,
    MongooseModule.forFeature(Schemas),
  ],
  exports: [MongooseModule, ConfigModule],
})
export class DomainModule {}
