import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CoordinateDocument = Coordinate & Document;

@Schema({ strict: false, timestamps: true })
export class Coordinate {
  createdAt: Date;
  updatedAt: Date;
}

export const CoordinatesSchema = SchemaFactory.createForClass(Coordinate);
