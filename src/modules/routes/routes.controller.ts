import { Body, Controller, Delete, Get, Param, Patch, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { CreateRouteDto, GetRoutesDto, UpdateRouteDto } from './dto';
import { RoutesService } from './routes.service';

@Controller('route')
export class RoutesController {
  constructor(private readonly routesService: RoutesService) {}

  @Get()
  async getRoute(@Query('address') address: string) {
    return this.routesService.getRoute(address);
  }

  @Get('list/:tab')
  async getRoutes(@Param('tab') tab: string, @Query() filter: GetRoutesDto) {
    return this.routesService.getRoutes(tab, filter);
  }

  @Put()
  @UseGuards(AuthGuard('jwt'))
  async saveRoute(@Body() dto: CreateRouteDto) {
    return this.routesService.saveRoute(dto);
  }

  @Patch()
  @UseGuards(AuthGuard('jwt'))
  async updateRouteInfo(@Body() dto: UpdateRouteDto) {
    return this.routesService.updateRouteInfo(dto);
  }

  @Delete()
  @UseGuards(AuthGuard('jwt'))
  async dropRoute(@Body('address') address: string) {
    return this.routesService.dropRoute(address);
  }
}
