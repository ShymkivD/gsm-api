var db = connect('mongodb://root:secure@localhost:27017/admin');

db.createUser({
  user: 'gsm-user',
  pwd: 'pass',
  roles: [{ role: 'readWrite', db: 'gsm' }],
  passwordDigestor: 'server',
});
