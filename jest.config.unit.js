module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: '.',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: ['**/*.(t|j)s'],
  coverageDirectory: './coverage/unit',
  testEnvironment: 'node',
  roots: ['<rootDir>/src/', '<rootDir>/libs/'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputName: '/coverage/unit/junit.xml',
        addFileAttribute: 'true',
      },
    ],
  ],
  coverageReporters: ['lcov', 'text', 'cobertura'],
  moduleNameMapper: {
    '@app/domain/(.*)': '<rootDir>/libs/domain/src/$1',
    '@app/domain': '<rootDir>/libs/domain/src',
    '@app/auth/(.*)': '<rootDir>/libs/auth/src/$1',
    '@app/auth': '<rootDir>/libs/auth/src',
  },
};
