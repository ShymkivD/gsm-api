import { CoordinatesModule } from './coordinates/coordinates.module';
import { RoutesModule } from './routes/routes.module';
import { UsersModule } from './users/users.module';

export const ApiModules = [CoordinatesModule, UsersModule, RoutesModule];
