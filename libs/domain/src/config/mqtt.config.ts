import { registerAs } from '@nestjs/config';
import { MqttClientOptions } from '@nestjs/microservices/external/mqtt-options.interface';

const { env } = process;
export const mqttConfig = registerAs('mqtt', () => ({
  url: env.MQTT_CONNECTION_URL ? env.MQTT_CONNECTION_URL : 'mqtt://localhost:1883',
  password: env.MQTT_PASSWORD,
  username: env.MQTT_USERNAME,
}));
