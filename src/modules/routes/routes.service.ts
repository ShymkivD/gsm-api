import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Route, RouteDocument } from '@app/domain/schemas';

import { CreateRouteDto, GetRoutesDto, UpdateRouteDto } from './dto';

@Injectable()
export class RoutesService {
  constructor(
    @InjectModel(Route.name)
    private readonly routesModel: Model<RouteDocument>,
  ) {}

  async getRoute(address: string) {
    const route = await this.routesModel.findOne({ address });

    if (!route) {
      throw new BadRequestException('Route not found');
    }

    return { route };
  }

  async getRoutes(tab: string, filter: GetRoutesDto) {
    let { max } = filter;
    const { min, shift, step, search } = filter;
    if (tab !== 'my') {
      tab = 'all';
    }

    max = min > max ? Number.MAX_VALUE : max;
    const query = this.routesModel.find({ distance: { $gt: min, $lte: max } }, null, {
      sort: { updatedAt: -1 },
      skip: shift,
      limit: step,
    });

    if (search.length >= 2) {
      query.where('title').regex(`^${filter.search}`);
    }

    const routes = await query;
    return { routes, filter, limits: this.distanceFilter(routes.length, { min, max }) };
  }

  async saveRoute(dto: CreateRouteDto) {
    const { address, distance, provider, route: points, title } = dto;
    const route = await this.routesModel.findOneAndUpdate(
      { address },
      { address, distance, provider, route: points, title },
      { upsert: true, new: true },
    );

    return { route };
  }

  async updateRouteInfo(dto: UpdateRouteDto) {
    const { address, title } = dto;
    const route = await this.routesModel.findOneAndUpdate({ address }, { title }, { new: true });

    return { route };
  }

  async dropRoute(address: string) {
    const route = await this.routesModel.findOneAndDelete({ address });

    return { route };
  }

  distanceFilter(itemsCount: number, { min: minimum, max: maximum }: { min: number; max: number }) {
    let min = Math.floor(minimum / 40) * 40;
    let max = Math.ceil(maximum / 40) * 40;

    if (max <= 0 || itemsCount == 0) {
      min = 0;
    } else if (min === max) {
      min = max - 40;
    } else if (max > 400) {
      max = 400;
    }
    return { min, max, count: itemsCount };
  }
}
