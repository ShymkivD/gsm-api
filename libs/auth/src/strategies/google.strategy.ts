import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Profile, Strategy, VerifyCallback } from 'passport-google-oauth20';

import { User } from '@app/domain/schemas';

import { AuthService } from '../auth.service';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private readonly authService: AuthService, configService: ConfigService) {
    super(configService.get('googleOAuth2'));
  }

  async validate(accessToken: string, refreshToken: string, profile: Profile, done: VerifyCallback) {
    const { id, displayName, photos, provider, emails } = profile;
    const userProfile: Partial<User> = {
      uid: `g:${id}`,
      name: displayName,
      email: emails[0].value,
      photo: photos[0].value,
      provider,
      token: accessToken,
    };

    const userWithToken = await this.authService.validateOAuthLogin(userProfile);
    done(null, userWithToken);
  }
}
