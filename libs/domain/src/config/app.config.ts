import { registerAs } from '@nestjs/config';

const { env } = process;
export const appConfig = registerAs('app', () => ({
  port: parseInt(env.PORT, 10) || 80,
  isProduction: env.NODE_ENV === 'production',
  jwtExpireTime: +env.JWT_EXPIRE_TIME || 3600,
  jwtSecret: env.JWT_SECRET,
}));
