import { Module } from '@nestjs/common';

import { CoordinatesController } from './coordinates.controller';
import { CoordinatesService } from './coordinates.service';

@Module({
  providers: [CoordinatesService],
  controllers: [CoordinatesController],
})
export class CoordinatesModule {}
