import { config } from 'dotenv';

import { appConfig } from './app.config';
import { mongoConfig } from './mongo.config';
import { mqttConfig } from './mqtt.config';
import { githubOAuth2Config, googleOAuth2Config } from './oauth2.config';
const oauth2 = [googleOAuth2Config, githubOAuth2Config];

config();

export const configs = [mongoConfig, appConfig, mqttConfig, ...oauth2];

export { appConfig, mongoConfig, mqttConfig };
export { githubOAuth2Config, googleOAuth2Config };
