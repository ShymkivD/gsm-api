import { registerAs } from '@nestjs/config';
import { StrategyOptions } from 'passport-google-oauth20';

const { env } = process;
export const googleOAuth2Config = registerAs(
  'googleOAuth2',
  () =>
    <any>{
      clientID: env.GOOGLE_CLIENT_ID,
      clientSecret: env.GOOGLE_SECRET,
      callbackURL: `http://localhost:${env.PORT}/auth/google/callback`,
      scope: ['profile', 'email'],
    },
);

export const githubOAuth2Config = registerAs(
  'githubOAuth2',
  () =>
    <any>{
      clientID: env.GITHUB_CLIENT_ID,
      clientSecret: env.GITHUB_CLIENT_SECRET,
      callbackURL: `http://localhost:${env.PORT}/auth/github/callback`,
      scope: ['read:user', 'user:email'],
    },
);
